from pathlib import Path 
import sys, hashlib

# INSERE NOS NOMES DOS ARQUIVOS DO DIRETORIO ESPECIFICADO OS HASHES MD5
# SINTAXE: python3 hashname.py <diretorio>

# EXTENSAO DOS ARQUIVOS QUE SERAO BUSCADOS
extensao = '*.zip'


def calc_md5hash(arquivo):
	hash_md5 = hashlib.md5()
	with open(arquivo, "rb") as f:
		for chunk in iter(lambda: f.read(4096), b""):
			hash_md5.update(chunk)
	return 'MD5-' + hash_md5.hexdigest()


def main():
	args = sys.argv
	caminho = args[1]

	arquivos = list(Path(caminho).glob(extensao))

	for a in arquivos:
		print(str(a))
		hash = calc_md5hash(str(a))
		arq = Path(str(a))
		arq.rename(str(a)[:-4] + '_' + hash + str(a)[-4:])
		# arq.rename(str(a)[:-37] + str(a)[-4:])


if __name__ == '__main__':
	main()
